/*eslint no-console:0 */
'use strict';
require('core-js/fn/object/assign');

const stormpath = require('koa-stormpath');
const convert = require('koa-convert');
const bodyParser = require('body-parser');
const json = require('koa-json');
const logger = require('koa-logger')
const koa = require('koa');
const sendfile = require('koa-sendfile')
const app = new koa();
const koaRouter = require('koa-router');
const router = new koaRouter();
const webpack = require('webpack');
const config = require('./webpack.config');
const serverConfig = require('config')
const compiler = webpack(config);
const WebpackDevServer = require('webpack-dev-server');
const open = require('open');

function failAndExit(err) {
  console.error(err.stack);
}

app.use(convert(logger()));
app.use(convert(json()));
app.use(require("webpack-dev-middleware")(compiler, config.devServer));

app.use(stormpath.init(app, {
  // Disable logging until startup, so that we can catch errors
  // and display them nicely.
  debug: 'none',
  apiKey: {
    id: 'YOUR_ID_HERE',
    secret: 'YOUR_SECRET_HERE'
  },
  application: {
    href: `YOUR_APP_HREF`
  },
  web: {
    produces: ['application/json'],
    me: {
      expand: {
        customData: true
      }
    },
    register: {
      form: {
        fields: {
          color: {
            enabled: true,
            label: 'Color',
            placeholder: 'E.g. blue',
            type: 'text'
          }
        }
      }
    }
  }
}));

router.post('/me', function (ctx,next) {
  console.log(ctx);
  function writeError(message) {
    ctx.status = 400 ;
    ctx.body ={
      message: message,
      status: 400
    };
  }

  function saveAccount() {
    ctx.user.givenName = ctx.body.givenName;
    ctx.user.surname = ctx.body.surname;
    ctx.user.email = ctx.body.email;

    if ('color' in ctx.body.customData) {
      ctx.user.customData.color = ctx.body.customData.color;
    }

    ctx.user.save(function(err) {
      if (err) {
        return writeError(err.userMessage || err.message);
      }
    });
  }

  if (ctx.body.password) {
    var application = this.settings.stormpathApplication;

    application.authenticateAccount({
      username: ctx.user.username,
      password: ctx.body.existingPassword
    }, function(err) {
      if (err) {
        return writeError('The existing password that you entered was incorrect.');
      }

      ctx.user.password = ctx.body.password;

      saveAccount();
    });
  } else {
    saveAccount();
  }
});

router.get('/', function(ctx,next) {
  ctx.body = sendFile(this, __dirname + 'src/index.html');
});

app.on('error', failAndExit);
app.on('stormpath.error', failAndExit);


app.listen(config.port, 'localhost', (err) => {
  if (err) {
    console.log(err);
  }
  console.log('Listening at localhost:' + config.port);
  console.log('Opening your system browser...');
  console.log(serverConfig)
  app.on('stormpath.ready', function() {
    console.log('\nListening at http://localhost:' + port);
    // Now bring back error logging.
    app.get('stormpathLogger').transports.console.level = 'error';
  });

  open('http://localhost:' + config.port + '/webpack-dev-server/');
});
